﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthController : ManagedGameObject
{
    private float depth = 0;
    private GameEntity gameEntity;

    [SerializeField]
    private AudioSource underWaterAmbianceSource, outSplashSource;

    private bool underWater = false;

    private void Awake()
    {
        gameEntity = GetComponent<GameEntity>();
    }
    
	private void Update ()
	{
	    depth = -transform.position.y;

        if (depth <= 0)
        {
            if (underWater)
            {
                outSplashSource.Play();
                underWaterAmbianceSource.Pause();
            }

            underWater = false;
            depth = 0;
            gameEntity.RefillPower();
        }

        else
        {
            if (!underWater)
            {
                underWaterAmbianceSource.Play();
                
                underWater = true;
            }
        }
            

	}

    public float GetCurrentDepth()
    {
        return depth;
    }
}
