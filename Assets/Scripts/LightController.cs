﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    [SerializeField] private Light directionalLight;
    [SerializeField] private Light spotLight;
    [SerializeField] private Light ambientLight;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Light sonarLight;
    [SerializeField] private float sonarPulseInterval = 3.0f;
    [SerializeField] private AudioSource sonarPulseSource;

    [SerializeField] private float noSpotLigthDepth = -20.0f;
    [SerializeField] private float spotLightDepth = -50.0f;
    
    private float ambientSurfaceRange;
    private float colorOriginalValue;
    private float saturationValue;
    private float hueValue;

    private bool isSpotUnlocked;
    private bool isSonarUnlocked;

    private Coroutine sonarCoroutine;
    
    private void Awake()
    {
        isSpotUnlocked = false;
        isSonarUnlocked = false;

        if (GameManager.IS_DEBUG_MODE)
        {
            isSpotUnlocked = true;
            isSonarUnlocked = true;
        }

        ambientSurfaceRange = ambientLight.range;

        Color.RGBToHSV( mainCamera.backgroundColor, out hueValue, out saturationValue, out colorOriginalValue );
    }

    private void Update()
    {
        float loc_current_depth = transform.position.y;
        float loc_current_lighted_depth = noSpotLigthDepth;

        spotLight.gameObject.SetActive( isSpotUnlocked );

        if (isSpotUnlocked)
        {
            loc_current_lighted_depth = spotLightDepth;
        }

        sonarLight.gameObject.SetActive( isSonarUnlocked );

        if (isSonarUnlocked)
        {
            if (sonarCoroutine == null)
            {
                sonarCoroutine = StartCoroutine( PlaySonarCoroutine() );
            }
        }

        ambientLight.range = ambientSurfaceRange - ( loc_current_depth / loc_current_lighted_depth) * ambientSurfaceRange;

        float loc_current_color_value = colorOriginalValue - ( loc_current_depth / loc_current_lighted_depth) * colorOriginalValue;
        mainCamera.backgroundColor = Color.HSVToRGB( hueValue, saturationValue, loc_current_color_value);

        
        directionalLight.intensity = 1 - ( loc_current_depth / loc_current_lighted_depth) * 1;
    }

    private IEnumerator PlaySonarCoroutine()
    {
        while ( isSonarUnlocked )
        {
            if (transform.position.y < noSpotLigthDepth)
            {
                sonarLight.gameObject.SetActive( true );
                sonarPulseSource.PlayDelayed(0.5f);
                yield return new WaitForSeconds( sonarPulseInterval );
            }

            sonarLight.gameObject.SetActive( false );
            
            yield return null;
        }

        sonarCoroutine = null;
    }

    public void UnlockSpot()
    {
        isSpotUnlocked = true;
    }

    public void UnlockSonar()
    {
        isSonarUnlocked = true;
    }
}
