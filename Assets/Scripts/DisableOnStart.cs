﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Disables the GameObject on Start()
 */
public class DisableOnStart : MonoBehaviour
{
    private void Start ()
	{
	    gameObject.SetActive(false);
	}
}
