﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SliderTextEditBinding : MonoBehaviour
{
    [SerializeField] private TMP_InputField inputField;
    [SerializeField] private Slider slider;

    private void Awake()
    {

    }

    public void OnInputValueChanged()
    {
        float value = 0;
        if (float.TryParse(inputField.text, out value))
        {
            if (value >= 0 && value <= 1)
            {
                slider.value = value;
            }

            else
            {
                SlideToTextInput();
            }
        }

        else
        {
            SlideToTextInput();
        }
        
    }

    public void OnSliderValueChanged()
    {
        SlideToTextInput();
    }

    private void SlideToTextInput()
    {
        inputField.text = String.Format("{0:F3}", slider.value);
    }
}
