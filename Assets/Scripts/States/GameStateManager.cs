﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class GameStateManager : MonoBehaviour
{
    private static readonly bool SHOW_DEBUG_LOG = true;

    private readonly Dictionary<Type, State> stateTable = new Dictionary<Type, State>();
    private State currentState;
    private State previousState;

    private void Awake()
    {
        InitialiseState<WelcomeState>();
        InitialiseState<MainMenuState>();
        InitialiseState<LevelSelectionState>();
        InitialiseState<SettingState>();
        InitialiseState<IngameState>();
        InitialiseState<GameOverState>();

        currentState = GetState<WelcomeState>();
    }

    private void Update()
    {
        currentState.UpdateState( Time.deltaTime );
    }

    private void InitialiseState<TYPE_STATE>() where TYPE_STATE : State
    {
        Type loc_state_type = typeof( TYPE_STATE );
        TYPE_STATE loc_state = new GameObject( loc_state_type.ToString() ).AddComponent<TYPE_STATE>();

        stateTable.Add( loc_state_type, loc_state );
        loc_state.transform.SetParent( transform, false );
    }

    public bool IsCurrentState<TYPE_STATE>() where TYPE_STATE : State
    {
        Type loc_state_type = typeof(TYPE_STATE);

        return (currentState != null) && (currentState.GetType() == loc_state_type);
    }

    public bool IsPreviousState<TYPE_STATE>() where TYPE_STATE : State
    {
        Type loc_state_type = typeof(TYPE_STATE);

        return (previousState != null) && (previousState.GetType() == loc_state_type);
    }

    public void ChangeState<TYPE_STATE>() where TYPE_STATE : State
    {
        Type loc_state_type = typeof(TYPE_STATE);

        Assert.IsTrue(stateTable.ContainsKey(loc_state_type));
        //Assert.AreNotEqual(currentState.GetType(), loc_state_type);

        currentState.Exit();

        if (SHOW_DEBUG_LOG)
        {
            Debug.LogFormat("ChangeState[<color=yellow>{0}</color>]->[<color=green>{1}</color>]", currentState.GetType(), loc_state_type);
        }

        previousState = currentState;
        currentState = GetState<TYPE_STATE>();
        currentState.Enter();
    }

    private TYPE_STATE GetState<TYPE_STATE>() where TYPE_STATE : State
    {
        Type loc_state_type = typeof(TYPE_STATE);

        return (TYPE_STATE) stateTable[loc_state_type];
    }

    public void RegisterOnEnter<TYPE_STATE>(Action par_event) where TYPE_STATE : State
    {
        State loc_state = GetState<TYPE_STATE>();

        loc_state.RegisterOnEnterEvent(par_event);
    }

    public void RegisterOnUpdate<TYPE_STATE>( Action par_event) where TYPE_STATE : State
    {
        State loc_state = GetState<TYPE_STATE>();

        loc_state.RegisterOnUpdateEvent(par_event);
    }

    public void RegisterOnExit<TYPE_STATE>(Action par_event) where TYPE_STATE : State
    {
        State loc_state = GetState<TYPE_STATE>();

        loc_state.RegisterOnExitEvent(par_event);
    }
}