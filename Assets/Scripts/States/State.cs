﻿using System;
using UnityEngine;

public abstract class State : MonoBehaviour
{
    private event Action OnEnterEvent;
    private event Action OnExitEvent;
    private event Action OnUpdateEvent;

    public virtual void Enter()
    {
        if ( OnEnterEvent != null )
        {
            OnEnterEvent();
        }
    }

    public virtual void UpdateState(float par_delta_time)
    {
        if (OnUpdateEvent != null)
        {
            OnUpdateEvent();
        }
    }

    public virtual void Exit()
    {
        if ( OnExitEvent != null )
        {
            OnExitEvent();
        }
    }

    public virtual bool HandleMessage(GameStateManager par_game_state_manager, string par_message)
    {
        return false;
    }

    public void RegisterOnEnterEvent(Action par_on_enter_event)
    {
        OnEnterEvent += par_on_enter_event;
    }

    public void RegisterOnUpdateEvent( Action par_on_update_event )
    {
        OnUpdateEvent += par_on_update_event;
    }

    public void RegisterOnExitEvent(Action par_on_exit_callback)
    {
        OnExitEvent += par_on_exit_callback;
    }
}
