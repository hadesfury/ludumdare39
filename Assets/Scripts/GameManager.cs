﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
#if UNITY_EDITOR
    public static readonly bool IS_DEBUG_MODE = true;
#else
    public static readonly bool IS_DEBUG_MODE = false;
#endif
    public enum GameState
    {
        WELCOME,
        MAIN_MENU,
        SETTINGS_MENU,
        INGAME,
        GAME_OVER,
        LEVEL_SELECTION
    }

    public enum GameOverCause
    {
        WIN,
        LOSE
    }

    [SerializeField] private LevelManager levelManager;

    [SerializeField] private GameEntity submarine;

    [SerializeField] private AudioSource destructionAudioSource;
    [SerializeField] private AudioSource blueprintAudioSource;

    [SerializeField] private GameObject blueprintLampText;
    [SerializeField] private GameObject blueprintSonarText;

    private const string MAIN_SCENE_NAME = "main";

    private GameOverCause gameOverCause;

    private int currentCoinCount = 0;
    private int coinCollectedCount = 0;

    private GameStateManager gameStateManager;

    private void Awake()
    {
        gameStateManager = GetComponentInChildren<GameStateManager>();

        ResetData();

        blueprintLampText.SetActive( false );
        blueprintSonarText.SetActive( false );

        levelManager.Initialise();
    }

    public LevelManager GetLevelManager()
    {
        return levelManager;
    }

    private void ResetData()
    {
        currentCoinCount = 0;

        if ( GameManager.IS_DEBUG_MODE )
        {
            currentCoinCount = 10000;
        }

        coinCollectedCount = 0;
    }

    public void ScoreBox( BoxComponent par_carried_box )
    {
        if (gameStateManager.IsCurrentState<IngameState>())
        {
            if ( par_carried_box.GetContentType() == BoxComponent.BoxContentType.BLUEPRINT )
            {
                blueprintAudioSource.Play();

                if ( par_carried_box.GetUpgradeType() == UpgradeType.LIGHT )
                {
                    submarine.GetComponent<LightController>().UnlockSpot();
                    blueprintLampText.SetActive( true );
                }
                else if ( par_carried_box.GetUpgradeType() == UpgradeType.SONAR )
                {
                    submarine.GetComponent<LightController>().UnlockSonar();
                    blueprintSonarText.SetActive( true );
                }
            }

            currentCoinCount += par_carried_box.GetValue();
            coinCollectedCount += par_carried_box.GetValue();

            GameObject.Destroy( par_carried_box.gameObject );

            if ( par_carried_box.GetContentType() == BoxComponent.BoxContentType.VICTORY )
            {
                TriggerGameOver( GameOverCause.WIN );
            }
        }
    }

    public int GetCoinCount()
    {
        return currentCoinCount;
    }

    public void Pay( int par_price )
    {
        currentCoinCount -= par_price;
        submarine.RefillPower();
        submarine.RepairHull();
    }

    public GameStateManager GetGameStateManager()
    {
        return gameStateManager;
    }

    private void Update()
    {
        if (gameStateManager.IsCurrentState<WelcomeState>())
        {
            if ( Input.GetButtonDown( "ReleaseGrapnel" ) )
            {
                GoToMainMenu();
            }
        }

        if (gameStateManager.IsCurrentState<GameOverState>())
        {
            if ( Input.GetKeyUp( KeyCode.Space ) )
            {
                SceneManager.LoadScene( MAIN_SCENE_NAME );
            }
        }
    }

    public void TriggerGameOver(GameOverCause par_game_over_cause)
    {
        gameOverCause = par_game_over_cause;

        if (gameStateManager.IsCurrentState<IngameState>())
        {
            gameStateManager.ChangeState<GameOverState>();

            destructionAudioSource.Play();

            levelManager.CompleteLoadedLevel(par_game_over_cause);
        }
    }

    public GameOverCause GetGameOverCause()
    {
        return gameOverCause;
    }

    public void Play()
    {
        gameStateManager.ChangeState<IngameState>();

        //submarine.Reset();
    }

    public void GoToSettingsMenu()
    {
        gameStateManager.ChangeState<SettingState>();
    }

    public void GoToMainMenu()
    {
        gameStateManager.ChangeState<MainMenuState>();
    }

    public void GoToLevelSelection()
    {
        gameStateManager.ChangeState<LevelSelectionState>();
    }

    public void Exit()
    {
        Application.Quit();
    }
}