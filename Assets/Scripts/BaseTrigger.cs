﻿using UnityEngine;
using System.Collections;

public class BaseTrigger : ManagedGameObject
{
    private GameEntity gameEntity;

    private void Update()
    {
        if( gameEntity != null && gameEntity.transform.position.y >= 0)
        {
            //if (loc_game_entity.GetComponent<DepthController>().GetCurrentDepth() > 0)
            {
                BoxComponent loc_current_box = gameEntity.GetCurrentBox();

                if( loc_current_box != null )
                {
                    gameManager.ScoreBox( loc_current_box );
                }

                gameEntity.RepairHull();
            }
        }
    }

    private void OnTriggerEnter( Collider par_collider )
    {
        gameEntity = par_collider.GetComponentInParent<GameEntity>();
    }

    private void OnTriggerStay(Collider par_collider) //Corrects a bug that suddenly appeared... which made the GUI not appearing on game start
    {
        gameEntity = par_collider.GetComponentInParent<GameEntity>();
    }

    private void OnTriggerExit( Collider par_collider )
    {
        GameEntity loc_game_entity = par_collider.GetComponentInParent<GameEntity>();

        if ( loc_game_entity != null )
        {
            gameEntity = null;
        }
    }

    public GameEntity GetGameEntity()
    {
        return gameEntity;
    }
}
