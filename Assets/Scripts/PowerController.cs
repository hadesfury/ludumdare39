﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerController : UpgradableController
{
    [SerializeField] private float powerConsumptionRatio = 100.0f;

    private float currentPower;

    public void Consume( float par_magnitude )
    {
        currentPower -= par_magnitude / powerConsumptionRatio;
    }

    public float GetCurrentPower()
    {
        return currentPower;
    }

    public void Refill()
    {
        currentPower = GetUpgradeValue();
    }

    private void FixedUpdate()
    {
        if (gameManager.GetGameStateManager().IsCurrentState<IngameState>())
        {
            if (Input.GetKey(KeyCode.F2))
            {
                currentPower -= 60 * Time.fixedDeltaTime;
            }
        }

        currentPower = Mathf.Max(0, currentPower);

        if (currentPower <= 0)
        {
            gameManager.TriggerGameOver(GameManager.GameOverCause.LOSE);
        }
    }
}
