﻿public enum UpgradeType
{
    HULL,
    POWER_CAP,
    LIGHT,
    SONAR,
    VERTICAL_POWER
}
