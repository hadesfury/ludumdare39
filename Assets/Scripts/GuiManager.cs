﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.PostProcessing;
using UnityEngine.UI;

public class GuiManager : ManagedGameObject
{
    [SerializeField] private TMP_Text loadedLevelText;

    [Header( "Main Menu" )]
    [SerializeField] private RectTransform welcomeMenuPanel;
    [SerializeField] private RectTransform menuButtons;
    [SerializeField] private RectTransform pressKeyText;

    [Header( "Main Menu Buttons" )]
    [SerializeField] private Button playButton;
    [SerializeField] private Button levelSelectionButton;
    [SerializeField] private Button settingButton;
    [SerializeField] private Button exitButton;
    
    [Header( "Settings Menu" )]
    [SerializeField] private RectTransform settingsPanel;
    [SerializeField] private TMP_InputField nameInputField;
    [SerializeField] private Slider brightnessSlider;
    [SerializeField] private Slider volumeSlider;

    [Header( "Game Over" )]
    [SerializeField] private RectTransform gameOverPanel;
    [SerializeField] private RectTransform winPanel;
    [SerializeField] private RectTransform losePanel;

    [Header("Level Selection")]
    [SerializeField] private RectTransform levelSelectionPanel;
    [SerializeField] private GridLayoutGroup levelGridPanel;
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private LevelSelectionElementLayout levelSelectionElementLayoutPrefab;
    [SerializeField] private Button levelSelectionBackButton;

    [Header( "Top Bar" )]
    [SerializeField] private RectTransform topBarPanel;
    [SerializeField] private TMP_Text powerValueText;
    [SerializeField] private TMP_Text depthValueText;
    [SerializeField] private TMP_Text damageValueText;
    [SerializeField] private TMP_Text coinValueText;
    [SerializeField] private Image powerBar;
    [SerializeField] private Image hullBar;

    [Header( "Upgrade" )]
    [SerializeField] private RectTransform upgradePanel;
    [SerializeField] private RectTransform upgradeContentPanel;
    [SerializeField] private UpgradeElementLayout upgradeElementPrefab;
    [SerializeField] private Sprite xboxButtonSpriteA;
    [SerializeField] private Sprite xboxButtonSpriteB;
    [SerializeField] private Sprite xboxButtonSpriteX;
    [SerializeField] private Sprite xboxButtonSpriteY;

    [Header("Warnings")]
    [SerializeField] private RectTransform alertsPanel;
    [SerializeField] private Image powerWarningImage;
    [SerializeField] private Image hullWarningImage;
    
    [Header( "Misc" )]
    [SerializeField] private GameObject submarine;
    [SerializeField] private BaseTrigger subBase;
    [SerializeField] private PostProcessingProfile postProcessingProfile;

    private PowerController powerController;
    private DepthController depthController;
    private DamageController damageController;
    private MovementController movementController;

    private Animator upgradePanelAnimator;
    private Animator settingsPanelAnimator;

    private readonly Dictionary<UpgradeType, UpgradeElementLayout> upgradeElementLayoutTable = new Dictionary<UpgradeType, UpgradeElementLayout>();

    private readonly List<LevelSelectionElementLayout> levelSelectionElementLayoutTable = new List<LevelSelectionElementLayout>();

    private void Awake()
    {
        playButton.onClick.AddListener(OnPlayButton);
        levelSelectionButton.onClick.AddListener(OnLevelSelectionButton);
        settingButton.onClick.AddListener(OnSettingButton);
        exitButton.onClick.AddListener(OnExitButton);

        powerController = submarine.GetComponent<PowerController>();
        depthController = submarine.GetComponent<DepthController>();
        damageController = submarine.GetComponent<DamageController>();
        movementController = submarine.GetComponent<MovementController>();

        powerWarningImage.gameObject.SetActive(false);
        hullWarningImage.gameObject.SetActive(false);

        upgradePanelAnimator = upgradePanel.GetComponent<Animator>();
        settingsPanelAnimator = settingsPanel.GetComponent<Animator>();

        levelSelectionBackButton.onClick.AddListener(OnLevelSelectionBackButton);

        InitialiseUpgradePanel(UpgradeType.HULL, "UpgradeHull");
        InitialiseUpgradePanel(UpgradeType.POWER_CAP, "UpgradePower");
        InitialiseUpgradePanel(UpgradeType.VERTICAL_POWER, "UpgradeSpeed");

        DiscardSettings(); //Method also used to initialize settings from settings file

    }

    private void OnEnable()
    {
        RegisterStateManagement();
    }

    private void OnExitButton()
    {
        gameManager.Exit();
    }

    private void OnSettingButton()
    {
        gameManager.GoToSettingsMenu();
    }

    private void OnLevelSelectionButton()
    {
        gameManager.GoToLevelSelection();
    }

    private void OnPlayButton()
    {
        gameManager.Play();
    }

    private void RegisterStateManagement()
    {
        gameManager.GetGameStateManager().RegisterOnEnter<WelcomeState>(OnWelcomeStateEnter);
        gameManager.GetGameStateManager().RegisterOnEnter<MainMenuState>(OnMainMenuStateEnter);
        gameManager.GetGameStateManager().RegisterOnEnter<LevelSelectionState>(OnLevelSelectionStateEnter);
        gameManager.GetGameStateManager().RegisterOnEnter<IngameState>(OnIngameStateEnter);
        gameManager.GetGameStateManager().RegisterOnUpdate<IngameState>(OnIngameStateUpdate);
        gameManager.GetGameStateManager().RegisterOnEnter<SettingState>(OnSettingStateEnter);
        gameManager.GetGameStateManager().RegisterOnUpdate<SettingState>(OnSettingStateUpdate);
        gameManager.GetGameStateManager().RegisterOnEnter<GameOverState>(OnGameOverStateEnter);

        OnWelcomeStateEnter();
    }

    private void OnWelcomeStateEnter()
    {
        welcomeMenuPanel.gameObject.SetActive(true);
        pressKeyText.gameObject.SetActive(true);
        menuButtons.gameObject.SetActive(false);
        settingsPanel.gameObject.SetActive(true);

        topBarPanel.gameObject.SetActive(false);
        upgradePanel.gameObject.SetActive(false);
        gameOverPanel.gameObject.SetActive(false);
        alertsPanel.gameObject.SetActive(false);
        levelSelectionPanel.gameObject.SetActive(false);
    }

    private void OnMainMenuStateEnter()
    {
        welcomeMenuPanel.gameObject.SetActive(true);
        pressKeyText.gameObject.SetActive(false);
        menuButtons.gameObject.SetActive(true);
        settingsPanel.gameObject.SetActive(true);

        if (settingsPanelAnimator.GetCurrentAnimatorStateInfo(0).IsName("In"))
        {
            settingsPanelAnimator.Play("Out");
        }

        topBarPanel.gameObject.SetActive(false);
        upgradePanel.gameObject.SetActive(false);
        gameOverPanel.gameObject.SetActive(false);
        alertsPanel.gameObject.SetActive(false);
        levelSelectionPanel.gameObject.SetActive(false);
    }

    private void OnLevelSelectionStateEnter()
    {
        welcomeMenuPanel.gameObject.SetActive(false);
        pressKeyText.gameObject.SetActive(false);
        menuButtons.gameObject.SetActive(false);

        topBarPanel.gameObject.SetActive(false);
        upgradePanel.gameObject.SetActive(false);
        gameOverPanel.gameObject.SetActive(false);
        alertsPanel.gameObject.SetActive(false);

        RefreshLevelTable();

        levelSelectionPanel.gameObject.SetActive(true);
    }

    private void OnSettingStateEnter()
    {
        welcomeMenuPanel.gameObject.SetActive(true);
        pressKeyText.gameObject.SetActive(false);
        menuButtons.gameObject.SetActive(false);

        settingsPanelAnimator.Play("In");

        topBarPanel.gameObject.SetActive(false);
        upgradePanel.gameObject.SetActive(false);
        gameOverPanel.gameObject.SetActive(false);
        alertsPanel.gameObject.SetActive(false);
        levelSelectionPanel.gameObject.SetActive(false);
    }

    private void OnSettingStateUpdate()
    {
        ColorGradingModel.Settings loc_color_grading_settings = postProcessingProfile.colorGrading.settings;

        loc_color_grading_settings.basic.postExposure = (brightnessSlider.value - 0.5f) * 10;
        postProcessingProfile.colorGrading.settings = loc_color_grading_settings;

        AudioListener.volume = volumeSlider.value;
    }

    private void OnIngameStateEnter()
    {
        welcomeMenuPanel.gameObject.SetActive(false);
        pressKeyText.gameObject.SetActive(false);
        menuButtons.gameObject.SetActive(false);
        settingsPanel.gameObject.SetActive(false);

        topBarPanel.gameObject.SetActive(true);
        gameOverPanel.gameObject.SetActive(false);
        upgradePanel.gameObject.SetActive(true);
        alertsPanel.gameObject.SetActive(true);
        levelSelectionPanel.gameObject.SetActive(false);
    }

    private void OnIngameStateUpdate()
    {
        powerValueText.SetText(string.Format("{0:F0}/{1:F0}", powerController.GetCurrentPower(), powerController.GetUpgradeValue()));
        depthValueText.SetText(string.Format("Depth : {0:F1}", depthController.GetCurrentDepth()));
        damageValueText.SetText(string.Format("{0:F0}/{1:F0}", damageController.GetCurrentHull(), damageController.GetUpgradeValue()));
        coinValueText.SetText(string.Format("{0}", gameManager.GetCoinCount()));

        powerBar.fillAmount = (float) powerController.GetCurrentPower() / powerController.GetUpgradeValue();
        hullBar.fillAmount = (float) damageController.GetCurrentHull() / damageController.GetUpgradeValue();

        powerWarningImage.gameObject.SetActive(powerController.GetCurrentPower() <= powerController.GetUpgradeValue() / 2.0f);
        hullWarningImage.gameObject.SetActive(damageController.GetCurrentHull() <= damageController.GetUpgradeValue() / 2.0f);

        if (subBase.GetGameEntity() != null)
        {
            if (submarine.transform.position.y > 0)
            {
                upgradePanelAnimator.Play("UpState");
                upgradeElementLayoutTable[UpgradeType.HULL].UpdateData("Hull", damageController, xboxButtonSpriteY);
                upgradeElementLayoutTable[UpgradeType.POWER_CAP].UpdateData("Power", powerController, xboxButtonSpriteB);
                upgradeElementLayoutTable[UpgradeType.VERTICAL_POWER].UpdateData("Speed", movementController, xboxButtonSpriteA);
            }
            else
            {
                upgradePanelAnimator.Play("DownState");
            }
        }
    }

    private void OnGameOverStateEnter()
    {
        welcomeMenuPanel.gameObject.SetActive(false);
        pressKeyText.gameObject.SetActive(false);
        menuButtons.gameObject.SetActive(false);
        settingsPanel.gameObject.SetActive(false);

        topBarPanel.gameObject.SetActive(false);
        upgradePanel.gameObject.SetActive(false);
        gameOverPanel.gameObject.SetActive(true);
        alertsPanel.gameObject.SetActive(false);
        levelSelectionPanel.gameObject.SetActive(false);

        winPanel.gameObject.SetActive(gameManager.GetGameOverCause() == GameManager.GameOverCause.WIN);
        losePanel.gameObject.SetActive(gameManager.GetGameOverCause() == GameManager.GameOverCause.LOSE);

    }

    private void OnLevelSelectionBackButton()
    {
        Assert.IsTrue(gameManager.GetGameStateManager().IsCurrentState<LevelSelectionState>());

        gameManager.GoToMainMenu();
    }

    private void Update()
    {
        loadedLevelText.SetText(string.Format("Loaded level : {0}", gameManager.GetLevelManager().GetLoadedLevelName()));
    }

    private void RefreshLevelTable()
    {
        List<string> loc_level_table = levelManager.GetLevelTable();

        foreach (LevelSelectionElementLayout loc_level_selection_element_layout in levelSelectionElementLayoutTable)
        {
            ReturnPoolableObject(loc_level_selection_element_layout);
        }

        foreach (string loc_level in loc_level_table)
        {
            LevelSelectionElementLayout loc_level_selection_element_layout = GetPoolableObject(levelSelectionElementLayoutPrefab);

            levelSelectionElementLayoutTable.Add(loc_level_selection_element_layout);
            loc_level_selection_element_layout.transform.SetParent(levelGridPanel.transform, false);
            loc_level_selection_element_layout.Initialise(loc_level, levelManager);
        }
    }

    private TYPE_OBJECT GetPoolableObject<TYPE_OBJECT>(TYPE_OBJECT par_prefab) where TYPE_OBJECT : MonoBehaviour
    {
        TYPE_OBJECT loc_instance = GameObject.Instantiate(par_prefab);

        return loc_instance;
    }

    private void ReturnPoolableObject<TYPE_OBJECT>(TYPE_OBJECT par_instance) where TYPE_OBJECT : MonoBehaviour
    {
        GameObject.Destroy(par_instance.gameObject);
    }

    private void InitialiseUpgradePanel(UpgradeType par_upgrade_type, String par_virtual_button)
    {
        UpgradeElementLayout loc_upgrade_element = GameObject.Instantiate(upgradeElementPrefab);

        loc_upgrade_element.Initialise(gameManager, par_virtual_button);

        loc_upgrade_element.transform.SetParent(upgradeContentPanel.transform, false);

        upgradeElementLayoutTable.Add(par_upgrade_type, loc_upgrade_element);
    }

    public void SaveSettings()
    {
        if (String.IsNullOrEmpty(nameInputField.text))
        {
            nameInputField.text = PlayerPrefs.GetString("playerName", "default");
            PlayerPrefs.SetString("playerName", nameInputField.text);
        }

        else
        {
            PlayerPrefs.SetString("playerName", nameInputField.text);
        }

        PlayerPrefs.SetFloat("brightness", brightnessSlider.value);
        PlayerPrefs.SetFloat("volume", volumeSlider.value);
    }

    public void DiscardSettings()
    {
        nameInputField.text = nameInputField.text = PlayerPrefs.GetString("playerName", "default");
        brightnessSlider.value = PlayerPrefs.GetFloat("brightness", 0.5f);
        volumeSlider.value = PlayerPrefs.GetFloat("volume", 1.0f);

        ColorGradingModel.Settings colorGradingSettings = postProcessingProfile.colorGrading.settings;
        colorGradingSettings.basic.postExposure = (PlayerPrefs.GetFloat("brightness", 0.5f) - 0.5f) * 10;
        postProcessingProfile.colorGrading.settings = colorGradingSettings;

        AudioListener.volume = PlayerPrefs.GetFloat("volume", 1.0f);
    }
}