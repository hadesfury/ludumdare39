﻿using UnityEngine;
using System.Collections.Generic;

public class UpgradableController : ManagedGameObject
{
    [SerializeField] private List<int> upgradeValueTable;
    [SerializeField] private List<int> upgradePriceTable = new List<int> { 100, 200, 300, 400 };

    private int currentLevel;

    private void Awake()
    {
        ConcreteAwake();
    }

    protected virtual void ConcreteAwake()
    {
        if (GameManager.IS_DEBUG_MODE)
        {
            //currentLevel = upgradeValueTable.Count - 1;
        }
    }

    public int GetCurrentLevel()
    {
        return currentLevel;
    }

    public int GetMaxLevel()
    {
        return upgradeValueTable.Count;
    }

    public int GetUpgradeValue()
    {
        return upgradeValueTable[ currentLevel ];
    }

    public int GetNextUpgradePrice()
    {
        return upgradePriceTable[ currentLevel + 1 ];
    }

    public void Upgrade()
    {
        ++currentLevel;
    }
}
