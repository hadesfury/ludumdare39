﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeElementLayout : MonoBehaviour
{
    [SerializeField] private TMP_Text upgradeName;
    [SerializeField] private TMP_Text currentLevel;
    [SerializeField] private TMP_Text cost;
    [SerializeField] private Button buyButton;
    [SerializeField] private Image buyButtonIcon;
    [SerializeField] private AudioSource buySound;

    private GameManager gameManager;
    private string virtualButton;

    private UpgradableController upgradableController;

    private void Awake()
    {
        buyButton.onClick.AddListener(OnBuyButton);
    }

    public void Initialise(GameManager par_game_manager, string par_virtual_button)
    {
        gameManager = par_game_manager;
        virtualButton = par_virtual_button;
    }

    private void OnBuyButton()
    {
        if (upgradableController != null)
        {
            int loc_price = upgradableController.GetNextUpgradePrice();

            upgradableController.Upgrade();
            gameManager.Pay(loc_price);
            buySound.Play();
        }
    }

    public void UpdateData(string par_name, UpgradableController par_upgradable_controller, Sprite par_button_sprite)
    {
        bool loc_is_next_upgrade_available = (par_upgradable_controller.GetCurrentLevel() < par_upgradable_controller.GetMaxLevel() - 1);
        bool has_controller = false;
        
        foreach (string loc_joystick_name in Input.GetJoystickNames())
        {
            if (!string.IsNullOrEmpty(loc_joystick_name))
            {
                has_controller = true;
            }
            //Debug.Log(loc_joystick_name);
        }

        upgradableController = par_upgradable_controller;
        upgradeName.SetText(par_name);
        currentLevel.SetText(string.Format("Level {0}/{1}", par_upgradable_controller.GetCurrentLevel() + 1, par_upgradable_controller.GetMaxLevel()));

        if (loc_is_next_upgrade_available)
        {
            cost.SetText(string.Format("{0}", upgradableController.GetNextUpgradePrice()));
        }
        else
        {
            cost.SetText("- - -");
        }

        buyButton.interactable = loc_is_next_upgrade_available && (upgradableController.GetNextUpgradePrice() <= gameManager.GetCoinCount());

        buyButtonIcon.gameObject.SetActive(has_controller);
        buyButtonIcon.sprite = par_button_sprite;
        
        if (!string.IsNullOrEmpty(virtualButton))
        {
            if (Input.GetButtonDown(virtualButton))
            {
                if (buyButton.interactable)
                {
                    OnBuyButton();
                }
            }
        }
    }
}