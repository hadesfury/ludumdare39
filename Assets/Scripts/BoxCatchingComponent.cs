﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCatchingComponent : MonoBehaviour
{
    [SerializeField] private GameObject submarine;
    [SerializeField] private float spring = 20;
    [SerializeField] private float damper = 15;

    private Rigidbody submarineRigidbody;
    private FixedJoint fixedJoint;
    private SpringJoint springJoint;
    private BoxComponent currentBox;

    private void Awake()
    {
        submarineRigidbody = submarine.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (currentBox != null)
        {
            if (Input.GetButtonDown("ReleaseGrapnel"))
            {
                fixedJoint.breakForce = 0;
                springJoint.breakForce = 0;
                currentBox = null;
            }
        }
    }

    private void OnCollisionEnter(Collision par_collision)
    {
        if (currentBox == null)
        {
            foreach (ContactPoint loc_contact in par_collision.contacts)
            {
                if (loc_contact.otherCollider.gameObject.CompareTag( "Box" ))
                {
                    BoxComponent loc_box_component = loc_contact.otherCollider.GetComponent<BoxComponent>();

                    fixedJoint = gameObject.AddComponent<FixedJoint>();
                    fixedJoint.connectedBody = par_collision.rigidbody;
                    springJoint = par_collision.gameObject.AddComponent<SpringJoint>();
                    springJoint.connectedBody = submarineRigidbody;
                    springJoint.spring = spring;
                    springJoint.damper = damper;

                    currentBox = loc_box_component;
                    loc_box_component.GetHookedSound().Play();

                    break;
                }

                //Debug.Log("Contact ! Type : " + loc_contact.otherCollider);
            }
        }
    }

    public BoxComponent GetCarriedBox()
    {
        return currentBox;
    }
}
