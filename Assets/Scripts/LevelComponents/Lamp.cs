﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : TriggerableComponent {
	private Light light;


	private void Awake() 
	{
		light = GetComponent<Light>();
	}

	override public void Toggle(bool par_toggle)
	{
		Debug.Log("Lamp toggled : " + par_toggle);
		light.enabled = par_toggle;
	}
}
