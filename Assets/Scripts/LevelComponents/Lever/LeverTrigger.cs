﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverTrigger : MonoBehaviour {
	[SerializeField] private bool triggerOn;
	private TriggerComponent lever;

	private void Awake() 
	{
		Debug.Log("LeverTrigger Awake !");
		lever = transform.parent.gameObject.GetComponent<TriggerComponent>();
	}

	public void OnTriggerEnter(Collider par_collider) {
		lever.Toggle(triggerOn);
	}
}
