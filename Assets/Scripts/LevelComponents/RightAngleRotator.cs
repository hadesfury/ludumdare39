﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RightAngleRotator : MonoBehaviour
{
    public enum Rotation
    {
        _0,
        _90,
        _180,
        _270
    }

    [SerializeField] private Rotation rotation;
    
    private void OnEnable()
    {
        UpdateOrientation();
    }

    private void Update()
    {
#if UNITY_EDITOR
        UpdateOrientation();
#endif
    }

    private void UpdateOrientation()
    {
        transform.localRotation = Quaternion.Euler(0, 0, 90*rotation.GetHashCode());
    }
}
