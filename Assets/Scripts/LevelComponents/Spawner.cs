﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * A triggerrable that instantiates a new instance of a specific object whenever it is triggered.
 */
public class Spawner : TriggerableComponent
{
    /**
     * The type of objects instantiated by the Spawner.
     */
    [SerializeField] private BoxType objectProduced;

    /**
     * An enumeration of all possible types of objects instantiated by a Spawner.
     */
    enum BoxType
    {
        WoodenBox,
        MetallicBox,
        TNTBox,
        Treasure
    };

    /**
     * The original transform of the instantiated objects.
     */
    [SerializeField] private Transform SpawnTransform;

    private Object asset;

    public void Awake()
    {
        asset = Resources.Load("Movables/"+objectProduced.ToString());
    }

    public override void Trigger()
    {
        GameObject instanciatedObject = Instantiate(asset, SpawnTransform.position, SpawnTransform.rotation, SpawnTransform) as GameObject;
        instanciatedObject.transform.SetParent(null);
    }

    public override void Toggle(bool loc_toggle)
    {
        if (loc_toggle)
        {
            Trigger();
        }
    }
}
