﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalDoubleTrap : TriggerableComponent
{
    [SerializeField] private GameObject trap1;
    [SerializeField] private GameObject trap2;

    private Rigidbody trap1Rigidbody;
    private Rigidbody trap2Rigidbody;

    public void Awake()
    {
        trap1Rigidbody = trap1.GetComponent<Rigidbody>();
        trap2Rigidbody = trap2.GetComponent<Rigidbody>();
    }

    public override void Trigger()
    {
        trap1Rigidbody.isKinematic = false;
        trap2Rigidbody.isKinematic = false;
    }

    public override void Toggle(bool loc_toggle)
    {
        if(loc_toggle) 
        {
            Trigger();
        }
            
    }
}
