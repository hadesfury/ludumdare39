﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Had to name it "PushButton" instead of "Button" for conflict reasons with GUI classes

public class PushButton : TriggerComponent
{

    [SerializeField] private GameObject buttonGameObject;
    [SerializeField] private bool timedButton;
    [SerializeField] private float time = 0f;

    private Rigidbody buttonRigidbody;

    private void Awake()
    {
        buttonRigidbody = buttonGameObject.GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider par_collider)
    {
        if (par_collider.gameObject == buttonGameObject)
        {
            LockButton();
            if (timedButton && time > 0f)
            {
                Invoke("UnlockButton", time);
            }
            Toggle(true);
        }
    }

    private void OnTriggerExit(Collider par_collider)
    {
        if (par_collider.gameObject == buttonGameObject)
        {
            Toggle(false);
        }
    }


    public void UnlockButton()
    {
        buttonRigidbody.isKinematic = false;
    }

    public void LockButton()
    {
        buttonRigidbody.isKinematic = true;
    }
}
