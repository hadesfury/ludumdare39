﻿using UnityEngine;
using System.Collections;

public class CaveEntrance : MonoBehaviour
{
    [SerializeField] private GameObject exit;

    public bool travelled = false;

    private void OnTriggerEnter(Collider par_collider)
    {
        if (exit == null || travelled)
            return;

        GameEntity entity = par_collider.GetComponentInParent<GameEntity>();
        entity.transform.position = exit.transform.position;
        exit.GetComponent<CaveEntrance>().travelled = true;
    }

    private void OnTriggerExit(Collider par_collider)
    {
        travelled = false;
    }
}
