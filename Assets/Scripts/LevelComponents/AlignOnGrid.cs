﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AlignOnGrid : MonoBehaviour
{
    [SerializeField] private float GridSizeX = 3, GridSizeY = 3, OffsetX, OffsetY;

    private void OnEnable()
    {
        UpdateAlignment();
    }

    private void Update()
    {
        UpdateAlignment();
    }

    private void UpdateAlignment()
    {
        if (!Application.isPlaying)
        {
            transform.position = new Vector3(transform.position.x - (transform.position.x + OffsetX) % GridSizeX,
                                             transform.position.y - (transform.position.y + OffsetY) % GridSizeY,
                                             0);
        }
    }
}
