﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PressurePlate : TriggerComponent
{

    [SerializeField] private GameObject buttonGameObject;

    private void OnTriggerEnter(Collider par_collider)
    {
        if (par_collider.gameObject == buttonGameObject)
        {
            Debug.Log("Pressure plate pressed");
            Toggle(true);
        }
    }

    private void OnTriggerExit(Collider par_collider)
    {
        if (par_collider.gameObject == buttonGameObject)
        {
            Toggle(false);
        }
    }

}
