﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

[ExecuteInEditMode]
public class SlidingDoor : TriggerableComponent
{
    [SerializeField] private GameObject door;
    [SerializeField] private GameObject base2;
    [SerializeField] private float size = 2;
    [SerializeField] private bool updateInRealTime = true;

    

    private void Awake()
    {
        EditSize();
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (updateInRealTime)
        {
            EditSize();
        }
#endif

        
    }

    public override void Toggle(bool par_toggle_open)
    {
        if (par_toggle_open)
        {
            OpenDoor();
        }

        else
        {
            CloseDoor();
        }
    }

    private void EditSize()
    {
        Vector3 loc_base2Position = base2.transform.localPosition;
        loc_base2Position.y = .5f + size;
        base2.transform.localPosition = loc_base2Position;

        Vector3 loc_doorSize = door.transform.localScale;
        loc_doorSize.y = size;
        Vector3 loc_doorPosition = door.transform.localPosition;
        loc_doorPosition.y = size / 2;

        door.transform.localPosition = loc_doorPosition;
        door.transform.localScale = loc_doorSize;
    }

    private void OpenDoor()
    {
        updateInRealTime = false;
        door.transform.DOLocalMoveY(-door.transform.lossyScale.y / 2, 1f);
    }

    private void CloseDoor()
    {
        updateInRealTime = false;
        door.transform.DOLocalMoveY(door.transform.lossyScale.y / 2, 1f);
    }
}
