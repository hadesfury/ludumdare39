﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalTrap : TriggerableComponent
{
    [SerializeField] private GameObject trap;

    private Rigidbody trapRigidbody;


    public void Awake()
    {
        trapRigidbody = trap.GetComponent<Rigidbody>();
    }

    public override void Trigger()
    {
        trapRigidbody.isKinematic = false;
    }

    public override void Toggle(bool loc_toggle)
    {
        if(loc_toggle)
        {
            Trigger();
        }
    }
}
