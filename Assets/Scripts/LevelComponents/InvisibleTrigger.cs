﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleTrigger : TriggerComponent {

    void OnTriggerEnter(Collider par_collision)
    {
        Debug.Log("Trigger entered : " + par_collision.gameObject.tag);
        if (par_collision.gameObject.layer == 13) //13 = submarine layer
        {
            Toggle(true);
        }
    }

    void OnTriggerExit(Collider par_collision)
    {
        if (par_collision.gameObject.layer == 13)
        {
            Toggle(false);
        }
    }
}
