﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerComponent : MonoBehaviour
{
    [SerializeField] private TriggerableComponent[] triggerableComponents;

    public void Trigger()
    {
        foreach (TriggerableComponent loc_triggerable_component in triggerableComponents)
        {
            loc_triggerable_component.Trigger();
        }
    }

    public void Toggle(bool par_toggle)
    {
        foreach (TriggerableComponent loc_triggerable_component in triggerableComponents)
        {
            loc_triggerable_component.Toggle(par_toggle);
        }
    }
}
