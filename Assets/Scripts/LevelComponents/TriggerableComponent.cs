﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TriggerableComponent : MonoBehaviour
{
    public virtual void Trigger()
    {

    }

    public virtual void Toggle(bool par_toggle)
    {

    }
}
