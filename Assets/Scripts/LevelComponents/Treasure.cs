﻿using UnityEngine;
using System.Collections;

public class Treasure : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.transform.root.CompareTag("Player"))
            Destroy(gameObject);
    }
}
