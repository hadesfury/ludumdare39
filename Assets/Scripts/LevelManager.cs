﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class LevelManager : ManagedGameObject
{
    [SerializeField] private List<string> levelTable;

    // use playerPref to keep track of scores by level name
    // each level have stars and a time ?

    // auto layout level screen

    private string loadedLevelName;
    
    public List<string> GetLevelTable()
    {
        return levelTable;
    }

    public void SelectLevel( string par_level_name )
    {
        LoadLevel( par_level_name );

        gameManager.Play();
    }

    private void LoadLevel(string par_level_name)
    {
        Assert.IsTrue(levelTable.Contains(par_level_name));

        if (!string.IsNullOrEmpty(loadedLevelName))
        {
            SceneManager.UnloadSceneAsync(loadedLevelName);
        }

        loadedLevelName = par_level_name;

        SceneManager.LoadScene(par_level_name, LoadSceneMode.Additive);
    }

    public void Initialise()
    {
         #if !UNITY_EDITOR
            Assert.IsTrue(SceneManager.sceneCount == 1);
            LoadLevel( levelTable[0] );
        #else
        if (SceneManager.sceneCount == 1)
        {
            LoadLevel( levelTable[0] );
        }
        #endif
    }

    public string GetLoadedLevelName()
    {
        return loadedLevelName;
    }

    public bool IsLevelCompleted(string par_level_name)
    {
        return PlayerPrefs.GetInt(string.Format("{0}_LEVEL_COMPLETED", par_level_name)) == 1;
    }
    
    public void CompleteLoadedLevel(GameManager.GameOverCause par_game_over_cause)
    {
        if (par_game_over_cause == GameManager.GameOverCause.WIN)
        {
            PlayerPrefs.SetInt(string.Format("{0}_LEVEL_COMPLETED",loadedLevelName), 1);
        }
    }
}