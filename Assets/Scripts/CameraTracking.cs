﻿using UnityEngine;

public class CameraTracking : ManagedGameObject
{
    [SerializeField] private GameObject submarine;
    
    private Vector3 trackingPosition;
    private Vector3 currentVelocity;
    private Vector3 currentRotationVelocity;
    
    private void FixedUpdate()
    {
        trackingPosition = new Vector3( submarine.transform.position.x, submarine.transform.position.y, -10 );

        if (gameManager.GetGameStateManager().IsCurrentState<IngameState>())
        {
            if (submarine.transform.position.y < 0)
            {
                transform.position = new Vector3(
                    Mathf.SmoothDamp(transform.position.x, trackingPosition.x, ref currentVelocity.x, 0.2f),
                    Mathf.SmoothDamp(transform.position.y, trackingPosition.y, ref currentVelocity.y, 0.2f),
                    Mathf.SmoothDamp(transform.position.z, -10, ref currentVelocity.z, 0.2f)
                );

                    transform.rotation = Quaternion.Euler(
                    Mathf.SmoothDampAngle(transform.rotation.eulerAngles.x, 0, ref currentRotationVelocity.x, 0.2f),
                    Mathf.SmoothDampAngle(transform.rotation.eulerAngles.y, 0, ref currentRotationVelocity.y, 0.2f),
                    Mathf.SmoothDampAngle(transform.rotation.eulerAngles.z, 0, ref currentRotationVelocity.z, 0.2f)
                );
            }

            else
            {
                transform.position = new Vector3(
                    Mathf.SmoothDamp(transform.position.x, trackingPosition.x, ref currentVelocity.x, 1.0f), 
                    Mathf.SmoothDamp(transform.position.y, 3, ref currentVelocity.y, 1.0f), 
                    Mathf.SmoothDamp(transform.position.z, -5, ref currentVelocity.z, 1.0f)
                );

                transform.rotation = Quaternion.Euler(
                    Mathf.SmoothDampAngle(transform.rotation.eulerAngles.x, 14, ref currentRotationVelocity.x, 1.0f),
                    Mathf.SmoothDampAngle(transform.rotation.eulerAngles.y, 0, ref currentRotationVelocity.y, 1.0f),
                    Mathf.SmoothDampAngle(transform.rotation.eulerAngles.z, 0, ref currentRotationVelocity.z, 1.0f)
                );
            }
        }

        if (gameManager.GetGameStateManager().IsCurrentState<MainMenuState>())
        {
            transform.position = new Vector3(
                Mathf.SmoothDampAngle(transform.position.x, -11.52f, ref currentVelocity.x, 1.0f),
                Mathf.SmoothDampAngle(transform.position.y, 0.56f, ref currentVelocity.y, 1.0f), 
                Mathf.SmoothDampAngle(transform.position.z, -1.11f, ref currentVelocity.z, 1.0f)
            );

            transform.rotation = Quaternion.Euler(
                Mathf.SmoothDampAngle(transform.rotation.eulerAngles.x, 0.0f, ref currentRotationVelocity.x, 1.0f),
                Mathf.SmoothDampAngle(transform.rotation.eulerAngles.y, 19.126f, ref currentRotationVelocity.y, 1.0f),
                Mathf.SmoothDampAngle(transform.rotation.eulerAngles.z, 0.0f, ref currentRotationVelocity.z, 1.0f)
            );
        }

        if (gameManager.GetGameStateManager().IsCurrentState<SettingState>())
        {
            transform.position = new Vector3(
                Mathf.SmoothDamp(transform.position.x, -8.40f, ref currentVelocity.x, 1.0f),
                Mathf.SmoothDamp(transform.position.y, 0.44f, ref currentVelocity.y, 1.0f),
                Mathf.SmoothDamp(transform.position.z, -2.1f, ref currentVelocity.z, 1.0f)
            );

            transform.rotation = Quaternion.Euler(
                Mathf.SmoothDampAngle(transform.rotation.eulerAngles.x, 0.0f, ref currentRotationVelocity.x, 1.0f),
                Mathf.SmoothDampAngle(transform.rotation.eulerAngles.y, -24.1f, ref currentRotationVelocity.y, 1.0f),
                Mathf.SmoothDampAngle(transform.rotation.eulerAngles.z, 0.0f, ref currentRotationVelocity.z, 1.0f)
            );
        }

        if (gameManager.GetGameStateManager().IsCurrentState<WelcomeState>())
        {
            transform.position = new Vector3(
                Mathf.SmoothDamp(transform.position.x, -7.46f, ref currentVelocity.x, 1.0f),
                Mathf.SmoothDamp(transform.position.y, 1.89f, ref currentVelocity.y, 1.0f),
                Mathf.SmoothDamp(transform.position.z, -4.7f, ref currentVelocity.z, 1.0f)
            );

            transform.rotation = Quaternion.Euler(
                Mathf.SmoothDampAngle(transform.rotation.eulerAngles.x, 7.629f, ref currentRotationVelocity.x, 1.0f),
                Mathf.SmoothDampAngle(transform.rotation.eulerAngles.y, -12.816f, ref currentRotationVelocity.y, 1.0f),
                Mathf.SmoothDampAngle(transform.rotation.eulerAngles.z, -0.474f, ref currentRotationVelocity.z, 1.0f)
            );
        }
    }
}
