﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : UpgradableController
{
    [SerializeField] private float maxForceApplyHeight = 0.35f;
    [SerializeField] private float hardHeightLimit = 0.35f;
    [SerializeField] private float minConsumePowerMagnitude = 0.1f;
    [SerializeField] private Transform meshAndCollisionTransform;

    private Rigidbody submarineRigidbody;
    private PowerController powerController;
    private DamageController damageController;
    private Animator animator;

    protected override void ConcreteAwake()
    {
        Vector3 loc_new_position = transform.position;

        loc_new_position.y = hardHeightLimit;
        transform.position = loc_new_position;

        submarineRigidbody = GetComponent<Rigidbody>();
        powerController = GetComponent<PowerController>();
        damageController = GetComponent<DamageController>();
        animator = GetComponentInChildren<Animator>();

        base.ConcreteAwake();
    }

    private void FixedUpdate()
    {
        if (gameManager.GetGameStateManager().IsCurrentState<IngameState>())
        {
            float loc_horizontal_axis = Input.GetAxis("Horizontal");
            float loc_vertical_axis = Input.GetAxis("Vertical");
            Vector3 loc_trust_vector = new Vector3( loc_horizontal_axis, loc_vertical_axis, 0);
            float loc_speed = GetUpgradeValue();
            float loc_horizontal_movement = loc_horizontal_axis * Time.fixedDeltaTime * loc_speed;
            float loc_vertical_movement = loc_vertical_axis * Time.fixedDeltaTime * loc_speed;
            Vector3 loc_movement_vector = new Vector3(loc_horizontal_movement, loc_vertical_movement, 0);

            //Debug.Log( loc_movement_vector );
        
            if (loc_trust_vector.magnitude > minConsumePowerMagnitude)
            {
                powerController.Consume(loc_trust_vector.magnitude);
            }

            //transform.Translate(loc_movement_vector);

            if (transform.position.y > maxForceApplyHeight)
            {
                submarineRigidbody.AddForce(-Vector3.up * loc_movement_vector.magnitude);
                submarineRigidbody.AddForce(-Vector3.up * 9.81f * Time.fixedDeltaTime);
            }
            else
            {
                submarineRigidbody.AddForce(loc_movement_vector);
            }

            animator.SetFloat("speed", submarineRigidbody.velocity.magnitude);
            animator.SetFloat("horizontalSpeed", submarineRigidbody.velocity.x);
            animator.SetFloat("horizontalAxis", loc_horizontal_axis);
            animator.SetFloat("absHorizontalSpeed", Mathf.Abs(submarineRigidbody.velocity.x));
            animator.SetFloat("verticalSpeed", submarineRigidbody.velocity.y);
            animator.SetFloat("verticalAxis", loc_vertical_axis);
            animator.SetFloat("absVerticalSpeed", Mathf.Abs(submarineRigidbody.velocity.y));

//            float halfTurnOffset = 0;
//            float subXVelocity = Mathf.Abs(submarineRigidbody.velocity.x);
//            if (subXVelocity < 0.5f && subXVelocity != 0 && Mathf.Abs(loc_horizontal_axis) > 0 )
//            {
//                    halfTurnOffset = 0.5f - subXVelocity;
//            }
//            Vector3 loc_look_at_position = new Vector3( halfTurnOffset, 0, -submarineRigidbody.velocity.x ).normalized;
//
//            meshAndCollisionTransform.LookAt(transform.position + loc_look_at_position);
//
//            if (Mathf.Abs(submarineRigidbody.velocity.x) > 0.1f)
//            {
//                if (submarineRigidbody.velocity.x > 0)
//                {
//                    meshAndCollisionTransform.RotateAround( meshAndCollisionTransform.transform.position, Vector3.forward, 10.0f * submarineRigidbody.velocity.y);
//                }
//                else
//                {
//                    meshAndCollisionTransform.RotateAround(meshAndCollisionTransform.transform.position, Vector3.forward, -10.0f * submarineRigidbody.velocity.y);
//                }
//            }
        }

        if (transform.position.y > hardHeightLimit)
        {
            Vector3 loc_new_position = transform.position;

            loc_new_position.y = hardHeightLimit;
            transform.position = loc_new_position;
        }
    }

    private void OnCollisionEnter(Collision par_collision)
    {
        Debug.Log("Submarine collision : " + par_collision.gameObject.name + " - Velocity : " + par_collision.relativeVelocity.magnitude);
        if( par_collision.gameObject.layer != LayerMask.NameToLayer( "SubGraple" ) )
        {
            damageController.Consume( par_collision.relativeVelocity.magnitude );
        }
    }
}
