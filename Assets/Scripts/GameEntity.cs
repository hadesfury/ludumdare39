﻿using UnityEngine;
using System.Collections;

public class GameEntity : MonoBehaviour
{
    private PowerController powerController;
    private DamageController damageController;
    private BoxCatchingComponent boxCatchingComponent;

    private void Awake()
    {
        const bool DOES_INCLUDE_INACTIVE = true;

        boxCatchingComponent = GetComponentInChildren<BoxCatchingComponent>(DOES_INCLUDE_INACTIVE);
        powerController = GetComponent<PowerController>();
        damageController = GetComponent<DamageController>();

        RefillPower();
        RepairHull();
    }

    public BoxComponent GetCurrentBox()
    {
        return boxCatchingComponent.GetCarriedBox();
    }

    public void RepairHull()
    {
        damageController.Repair();
    }

    public void RefillPower()
    {
        powerController.Refill();
    }
}
