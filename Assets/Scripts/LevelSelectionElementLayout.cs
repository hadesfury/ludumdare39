using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionElementLayout : MonoBehaviour
{
    [SerializeField] private TMP_Text levelNameText;
    [SerializeField] private Image successPanel;
    [SerializeField] private Button playButton;

    private string levelName;
    private LevelManager levelManager;

    private void Awake()
    {
        playButton.onClick.AddListener(OnPlayButton);
    }

    public void Initialise(string par_level_name, LevelManager par_level_manager)
    {
        levelManager = par_level_manager;
        levelName = par_level_name;

        levelNameText.SetText(par_level_name);
        successPanel.gameObject.SetActive(par_level_manager.IsLevelCompleted(par_level_name));
    }

    private void OnPlayButton()
    {
        levelManager.SelectLevel(levelName);
    }
}