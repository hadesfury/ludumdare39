﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxComponent : MonoBehaviour
{
    public enum BoxContentType
    {
        GOLD,
        BLUEPRINT,
        VICTORY,
        TNT
    }

    [SerializeField] private float mass = 1;
    [SerializeField] private float integrity = 100;
    [SerializeField] private float minDamageVelocity = 10;
    [SerializeField] private bool indestructible = false;
    [SerializeField] private BoxContentType boxContentType;

    [Header("Gold box")]
    [SerializeField] private float value = 10;

    [Header("Blueprint box")]
    [SerializeField] private UpgradeType upgradeType = UpgradeType.LIGHT;

    [SerializeField] private AudioSource hookedSound;
    [SerializeField] private AudioSource scoringSound;

    [Header("TNT box")]
    [SerializeField] private float explosionPower = 2f;
    [SerializeField] private float explosionRadius = 5f;
    [SerializeField] private AudioSource explosionSound;

    private new Rigidbody rigidbody;
    private GameObject collisionGameObject = null;
    private Vector3 collisionOffset;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.mass = mass;
    }

    private void OnCollisionEnter(Collision par_collision) 
    {
        if( par_collision.gameObject.CompareTag( "Door" ) )
        {
            collisionGameObject = par_collision.collider.gameObject;
            transform.parent = par_collision.collider.transform;
            rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        }

        //Detect damages on the box if it's not indestructible
        if (!indestructible) 
        {
            if(par_collision.relativeVelocity.magnitude >= minDamageVelocity) 
            {
                integrity -= par_collision.relativeVelocity.magnitude; //Apply damages to integrity

                //Debug.Log("Box : " + gameObject.name + " - Integrity = " + integrity);

                //Can also change appearance according to integrity ?
                
                if(integrity <= 0) 
                {
                    if(boxContentType == BoxContentType.TNT)
                    {
                        Explode();
                    }
                    else 
                    {
                        DestroyBox();
                    }
                }
            }
        }
    }

    private void OnCollisionStay( Collision par_collision )
    {
        
        
    }

    private void OnCollisionExit( Collision par_collision )
    {
        if( par_collision.collider.gameObject == collisionGameObject )
        {
            collisionGameObject = null;
            transform.parent = null;
            rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        }
    }

    private void FixedUpdate()
    {
        //if( collisionGameObject != null )
        //{
        //    transform.position = collisionGameObject.transform.position + collisionOffset;
        //}
    }

    public int GetValue()
    {
        return Mathf.RoundToInt(value);

    }

    public BoxContentType GetContentType()
    {
        return boxContentType;
    }

    public UpgradeType GetUpgradeType()
    {
        return upgradeType;
    }

    public AudioSource GetHookedSound()
    {
        return hookedSound;
    }

    public AudioSource GetScoringSound()
    {
        return scoringSound;
    }

    private void Explode() 
    {
        explosionSound.Play();

        int loc_layer_mask = (1 << 8) + (1 << 13) + (1 << 14); //Include layer 8, 13 & 14 (Rope, Submarine & Boxes)
        Collider[] loc_hits = Physics.OverlapSphere(transform.position, explosionRadius, loc_layer_mask);
        
        //Debug.Log(loc_hits.Length);

        List<int> instances = new List<int>();

        foreach(Collider loc_hit in loc_hits)
        {
            if(!instances.Exists(x => x == loc_hit.gameObject.GetInstanceID())) 
            {
                instances.Add(loc_hit.gameObject.GetInstanceID());
                
                Rigidbody targetRigidbody = loc_hit.gameObject.GetComponent<Rigidbody>();
                //Debug.Log("Explosion hit : " + loc_hit.name + " - rigidbody : " + (targetRigidbody ? "true" : "false"));

                if(targetRigidbody)
                {
                    targetRigidbody.AddExplosionForce(explosionPower, transform.position, explosionRadius, 0.0f, ForceMode.Force);
                }
            }
        }

        GetComponent<BoxCollider>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;
        

        Destroy(gameObject, 1.3f);
    }

    private void DestroyBox() 
    {
        //float t = 0f; //Destruction delay, for the animation to have time to play.

        /*
        /*Include destruction sound and animation here 
         */
        
        Destroy(gameObject);

    }
}
