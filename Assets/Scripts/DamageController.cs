﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : UpgradableController
{
    [SerializeField] private float damageRatio = 1.0f;
    [SerializeField] private AudioSource impactSource;

    private float currentHullValue;

    private void Awake()
    {

    }

    public void Consume( float par_magnitude )
    {
        if (par_magnitude / damageRatio >= 5)
        {
            currentHullValue -= par_magnitude / damageRatio;

            if (currentHullValue > 0)
            {
                impactSource.Play();
            }
        }
    }

    public float GetCurrentHull()
    {
        return currentHullValue;
    }
    
    public void Repair()
    {
        currentHullValue = GetUpgradeValue();
    }

    private void FixedUpdate()
    {
        if (gameManager.GetGameStateManager().IsCurrentState<IngameState>())
        {
            if (Input.GetKey(KeyCode.F3))
            {
                currentHullValue -= 60 * Time.fixedDeltaTime;
            }
        }

        currentHullValue = Mathf.Max( 0, currentHullValue );

        if (currentHullValue <= 0)
        {
            gameManager.TriggerGameOver(GameManager.GameOverCause.LOSE);
        }
    }
}
